/*!
 * LeepFrog developer-project JS activity.
 *
 * requires jquery and jquery ui.
 * <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 * <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 * <script src="navbar.js"></script>
 *
 * Martin Wise
 * Date: 2018-12-12
 */

// use the json file as an array
var jarray = $.getJSON("navlinks.json");

function navSearch(campus, section) {
    // just finding the section requested.
    var obj = [];
    $.each(jarray.responseJSON, function (i, navelem) {
        if (navelem.campus.toLowerCase() == campus.toLowerCase() && navelem.section.toLowerCase() == section.toLowerCase()) {
            obj.push(navelem);
        }
    });
    return obj;
}

function navObjs(campus, section) {
    //split section into pieces, format each section.
    var sections = [];
    var obj = [];
    var nodename = '';
    var nextnodename = '';
    var thisobj = [];

    sections = section.split("/");
    sections = sections.slice(0, sections.length);
    //find root - make active
    $.each(sections, function (i, node) {
        //find node
        nodename = sections.slice(0, i).join("/") + "/";
        nextnodename = sections.slice(0, i + 1).join("/") + "/";
        thisobj = navSearch(campus, nodename);
        //identify active
        $.each(thisobj, function (i, piece) {
            if (piece.url == nextnodename) {
                piece["active"] = true;
            }
            else {
                //only needed if run twice in the same session.
                delete piece.active
            }

        });
        obj.push(thisobj);
    });
    //not sure why I get two of the first object.
    return obj.slice(1, obj.length);
}

function fullNavbar(campus, section) {
    //collect attributes
    var subhtml;
    var htmlstr;
    var objs = navObjs(campus, section);

    // construct basic html.  replace subdirs later.
    $.each(objs, function (i, item) {
        $.each(item, function (k, node) {
            node["html"] = "<li><a href=\"" + node.url + "\">" + node.name + "</a>" + "</li> "
        })
    });
    //work backwards through the array and replace html with 'active' html.
    $.each(objs.reverse(), function (i, items) {
        $.each(items, function (k, item) {
            if (item.active == true) {
                item.html = item.html.replace('<li>',"<li class=\"active\">") + "\n<ul>\n";
                 subhtml = [];
                //collect the html from the next level.
                $.each(objs[i-1], function (l, its) {
                    subhtml.push(its.html)
                });
                item.html = item.html + subhtml.join("\n") + "\n</ul>";
            }
        });
    });
    //put the array back in order.
    objs.reverse();

    //build the string
    htmlstr = "<ul>\n<li><a href=\"/\" title=\"$campus navigation\">Catalog Home</a></li>\n".replace('$campus', campus);
    subhtml = [];
    $.each(objs[0], function (i, item) {
        subhtml.push(item.html);
    });
    htmlstr = htmlstr + subhtml.join("\n");
    htmlstr = htmlstr + "</ul>";
    // here you go :)
    return htmlstr;

}
